package com.gitlab.retropronghorn.retrosutils.material;

import java.util.Arrays;
import java.util.List;
import org.bukkit.Material;

public class UPhasables {
    public static List<Material> phasableBlockTypes = Arrays.asList(
        Material.AIR,
        // Folliage
        Material.GRASS,
        Material.DEAD_BUSH,
        // Flowers
        Material.POPPY,
        Material.DANDELION,
        Material.SUNFLOWER,
        Material.BLUE_ORCHID,
        Material.ORANGE_TULIP,
        Material.WHITE_TULIP,
        Material.PINK_TULIP,
        Material.RED_TULIP
    );
}