package com.gitlab.retropronghorn.retrosutils.material;

import com.gitlab.retropronghorn.retrosutils.server.ULogger;

import org.bukkit.Material;

public class UMaterial {
    public static final Material FALLBACK = Material.DIRT;
    private static final Material[] MATERIALS = Material.values();

    /**
     * Return if a material exists
     * @param p String value of material
     * @return returns wether or not the material exists
     */
    public static Boolean exists(String p) {
        // Cache and iterate, it's faster.
        for (int i = 0; i < MATERIALS.length; i++) {
            if (MATERIALS[i].toString().equals(p)) return true;
        }
        return false;
    }

    /**
     * Return material from string or fallback if not exists
     * @param p String value of the material
     * @return returns a material from string or fallback material
     */
    public static Material materialOrFallback(String p) {
        if (!exists(p))
            ULogger.warning(
                "Retro's Utils",
                "Material " + p + " does not exist, falling back to default material."
            );
        return exists(p) ? Material.valueOf(p) : FALLBACK;
    }
}