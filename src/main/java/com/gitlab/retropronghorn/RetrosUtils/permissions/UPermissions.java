package com.gitlab.retropronghorn.retrosutils.permissions;

import java.util.function.Predicate;

import org.bukkit.entity.Player;

/** Utility methods for checking permissions
 * @author RetroPronghorn
 * @since 1.0
 */
public class UPermissions {

    /**
     * Check wether a player is op
     * @return wether a player is op
     */
    public static Predicate<? extends Player> isOp() {
        return p -> p.isOp();
    }

    /**
     * Check wether a player is not op
     * @return wether a player isnot  op
     */
    public static Predicate<? extends Player> isNotOp() {
        return p -> !p.isOp();
    }

    /**
     * Check wether a player is not op
     * @param player player to check
     * @return wether a player is not op
     */
    public static Boolean isNotOp(Player p) {
        return !p.isOp();
    }

    /**
     * Check wether a player has a permission node, or is op
     * @param p player to check
     * @param node permission node to check
     * @return returns wether a player has node or is op
     */
    public static Boolean hasPermOrOp(Player p, String node) {
        return p.hasPermission(node) || p.isOp();
    }
}