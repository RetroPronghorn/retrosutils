package com.gitlab.retropronghorn.retrosutils.block;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class UMultiblockParser {
    /**
     * Builds a three-dimentional array reperesenting a cube of selected blocks.
     * @param a First corner to start parsing at
     * @param b Last corner to finish parsing at
     * @return three-diemnsional array of the materials in selection.
     */
    public static List<List<List<Material>>> to3DMaterialList(Location a, Location b) {
        World w = a.getWorld();
        // Min-max
        Double yMax = (a.getY() > b.getY()) ? a.getY() : b.getY();
        Double xMax = (a.getX() > b.getX()) ? a.getX() : b.getX();
        Double zMax = (a.getZ() > b.getZ()) ? a.getZ() : b.getZ();
        Double yMin = (a.getY() < b.getY()) ? a.getY() : b.getY();
        Double xMin = (a.getX() < b.getX()) ? a.getX() : b.getX();
        Double zMin = (a.getZ() < b.getZ()) ? a.getZ() : b.getZ();
        // Build new location selections
        Location start = new Location(w, xMax, yMax, zMin);
        Location end = new Location(w, xMin, yMin, zMax);
        // Determine geometry
        Double length = Math.abs(end.getX() - start.getX())+1;
        Double width = Math.abs(start.getZ() - end.getZ())+1;
        Double height = Math.abs(start.getY() - end.getY())+1;

        List<List<List<Material>>> cube = IntStream.range(0, height.intValue())
            .mapToObj(i -> {
                Location oL = start.subtract(new Vector(0, i, 0));
                return parseLayer(oL, length, width);
            })
            .collect(Collectors.toList());

        return cube;
    }

    /**
     * Builds a list of materials representing a row of materials
     * Collects on the x axis
     * @param start location to start collecting
     * @param length total blocks to collect
     * @return list of materials in row
     */
    private static List<Material> parseRow(Location start, Double length) {
        List<Material> materials = IntStream.range(0, length.intValue())
            .mapToObj(i -> {
                Location oL = start.subtract(new Vector(i, 0, 0));
                return oL.getBlock().getType();
            })
            .collect(Collectors.toList());

        return materials;
    }

    /**
     * Builds a two-dimensional array representing a layer of blocks in a quadrilateral
     * @param start location to start parsing at
     * @param length length of the quadrilateral
     * @param width width of the quadrilateral
     * @return two-dimensional array of materials in selection
     */
    private static List<List<Material>> parseLayer(Location start, Double length, Double width) {
        List<List<Material>> layer = IntStream.range(0, width.intValue())
            .mapToObj(i -> {
                Location oL = start.add(new Vector(0, 0, i));
                return parseRow(oL, length);
            })
            .collect(Collectors.toList());

        return layer;
    }
}