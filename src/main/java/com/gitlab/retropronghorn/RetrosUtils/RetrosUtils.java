package com.gitlab.retropronghorn.retrosutils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class RetrosUtils extends JavaPlugin {
    private static RetrosUtils instance;

    /**
     * Get plugin instance
     **/
    public static RetrosUtils getPlugin() {
        return instance;
    }

    @Override
    public void onEnable() {
        // Info to console
        Bukkit.getConsoleSender().sendMessage(
            ChatColor.GREEN + "-------------------------" +"\n\n"+
            ChatColor.GRAY + "Loaded:" +"\n"+
                ChatColor.WHITE + "Retro's Utils Ver. " + getDescription().getVersion() +"\n"+
            ChatColor.GRAY + "By: " + getDescription().getAuthors() +"\n"+
            ChatColor.GRAY + "Built for 1.12-1.15.2+" +"\n\n"+
            ChatColor.GREEN + "-------------------------"
        );

        // Bind Instance
        instance = this;
    }
}
