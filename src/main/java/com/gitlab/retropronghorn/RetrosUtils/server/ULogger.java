package com.gitlab.retropronghorn.retrosutils.server;

import org.bukkit.Bukkit;

import net.md_5.bungee.api.ChatColor;

public class ULogger {
        /**
     * Wrap a string in the plugin formatter
     *
     * @param message message to wrap
     **/
    private static void wrap(String title, String message) {
        Bukkit.getConsoleSender().sendMessage("["+ChatColor.LIGHT_PURPLE+title+ChatColor.RESET+"]: "+message);
    }

    /**
     * Log to the info level
     *
     * @param message message to log
     **/
    public static void info(String title, String message) {
        wrap(title, ChatColor.WHITE + message);
    }

    /**
     * Log to the success level
     *
     * @param message message to log
     **/
    public static void success(String title, String message) {
        wrap(title, ChatColor.GREEN + message);
    }

    /**
     * Log to the warning level
     *
     * @param message message to log
     **/
    public static void warning(String title, String message) {
        wrap(title, ChatColor.YELLOW + message);
    }

    /**
     * Log to the error level
     *
     * @param message message to log
     **/
    public static void error(String title, String message) {
        wrap(title, ChatColor.RED + message);
    }
}