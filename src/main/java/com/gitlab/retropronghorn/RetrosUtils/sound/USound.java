package com.gitlab.retropronghorn.retrosutils.sound;

import com.gitlab.retropronghorn.retrosutils.server.ULogger;

import org.bukkit.Sound;

/** Utility methods for checking and building sounds
 * @author RetroPronghorn
 * @since 1.0
 */
public class USound {
    public static final Sound FALLBACK = Sound.ENTITY_EXPERIENCE_ORB_PICKUP;
    private static final Sound[] SOUNDS = Sound.values();

    /**
     * Return if a sound exists
     * @param p String value of sound
     * @return returns wether or not the sound exists
     */
    public static Boolean exists(String p) {
        // Cache and iterate, it's faster.
        for (int i = 0; i < SOUNDS.length; i++) {
            if (SOUNDS[i].toString().equals(p)) return true;
        }
        return false;
    }

    /**
     * Return sound from string or fallback if not exists
     * @param p String value of the sound
     * @return returns a sound from string or fallback sound
     */
    public static Sound soundOrFallback(String p) {
        if (!exists(p))
            ULogger.warning(
                "Retro's Utils",
                "Sound " + p + " does not exist, falling back to default sound."
            );
        return exists(p) ? Sound.valueOf(p) : FALLBACK;
    }
}