package com.gitlab.retropronghorn.retrosutils.player;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.gitlab.retropronghorn.retrosutils.permissions.UPermissions;

public class UPlayer {

    /**
     * Check if a player is in spectator mode
     * @param p player to check
     * @return wether or not the player is spectating
     */
    public static Boolean isSpectator(Player p) {
        return p.getGameMode() == GameMode.SPECTATOR;
    }

    /**
     * Kick all players from the server
     * @param reason reason for kicking players
     */
    public static void kickAll(String reason) {
        Bukkit.getOnlinePlayers().stream()
            .map(p -> (Player) p)
            .forEach(p -> p.kickPlayer(reason));
    }

    /**
     * Kick all players from the server who are not OP
     * @param reason reason for kicking players
     */
    public static void kickNonOP(String reason) {
        Bukkit.getOnlinePlayers().stream()
            .map(p -> (Player) p)
            .filter(UPermissions::isNotOp)
            .forEach(p -> p.kickPlayer(reason));
    }

    /**
     * Kicks all players from the server who are spectating
     * @param reason reason for kicking players
     */
    public static void kickSpectators(String reason) {
        Bukkit.getOnlinePlayers().stream()
            .map(p -> (Player) p)
            .filter(UPlayer::isSpectator)
            .forEach(p -> p.kickPlayer(reason));
    }

    /**
     * Slow down a player's velocity
     * @param p player to slow
     */
    public static void slowPlayer(Player p) {
        p.setVelocity(new Vector().zero());
    }

    /**
     * Slow down all player's velocity
     */
    public static void slowAllPlayers() {
        Bukkit.getOnlinePlayers().forEach(p -> slowPlayer(p));
    }

    /**
     * Unslow a player
     * @param p player to unslow
     */
    public static void unslowPlayer(Player p) {
        p.setVelocity(new Vector(1,1,1));
    }

    /**
     * Unslow all players
     */
    public static void unslowAllPlayers() {
        Bukkit.getOnlinePlayers().forEach(p -> unslowPlayer(p));
    }
}