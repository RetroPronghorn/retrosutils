package com.gitlab.retropronghorn.retrosutils.player;

import org.bukkit.entity.Player;

/** Utility methods for checking, setting, and removing experience
 * @author RetroPronghorn
 * @since 1.0
 */
public class UExperience {

    /**
     * Determine if a player has required experience
     *
     * @param requiredExp Required amount of experience
     * @return returns if the player has given required experience
     **/
    public static Boolean hasEnoughExperience(Player p, Double requiredExp) {
        return (getTotalExperience(p) >= requiredExp);
    }

    /**
     * Deduct experience from player
     *
     * @param amount Amount of experience to deduct
     **/
    public static void deductExperience(Player p, Double amount) {
        Integer reducedExp = getTotalExperience(p) - ((int) Math.round(amount));
        setTotalExperience(p, reducedExp);
    }

    /**
     * Get the total experience a player has
     *
     * @param level level of the player
     * @return returns the total experience by level
     **/
    private static int getTotalExperience(int level) {
        int xp = 0;

        if (level >= 0 && level <= 15) {
            xp = (int) Math.round(Math.pow(level, 2) + 6 * level);
        } else if (level > 15 && level <= 30) {
            xp = (int) Math.round((2.5 * Math.pow(level, 2) - 40.5 * level + 360));
        } else if (level > 30) {
            xp = (int) Math.round(((4.5 * Math.pow(level, 2) - 162.5 * level + 2220)));
        }
        return xp;
    }

    /**
     * Get the total experience a player has
     *
     * @return retruns the players total experience
     **/
    private static int getTotalExperience(Player p) {
        return Math.round(p.getExp() * p.getExpToLevel()) + getTotalExperience(p.getLevel());
    }

    /**
     * Set the total experience a player has
     *
     * @param amount amount of experience to set on the player
     **/
    private static void setTotalExperience(Player p, int amount) {
        int level = 0;
        int xp = 0;
        float a = 0;
        float b = 0;
        float c = -amount;

        if (amount > getTotalExperience(0) && amount <= getTotalExperience(15)) {
            a = 1;
            b = 6;
        } else if (amount > getTotalExperience(15) && amount <= getTotalExperience(30)) {
            a = 2.5f;
            b = -40.5f;
            c += 360;
        } else if (amount > getTotalExperience(30)) {
            a = 4.5f;
            b = -162.5f;
            c += 2220;
        }
        level = (int) Math.floor((-b + Math.sqrt(Math.pow(b, 2) - (4 * a * c))) / (2 * a));
        xp = amount - getTotalExperience(level);
        p.setLevel(level);
        p.setExp(0);
        p.giveExp(xp);
    }
}
