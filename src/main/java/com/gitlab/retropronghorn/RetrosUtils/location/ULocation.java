package com.gitlab.retropronghorn.retrosutils.location;

import java.util.function.Predicate;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

import com.gitlab.retropronghorn.retrosutils.material.UPhasables;

/** Utility methods for common location tasks
 * @author RetroPronghorn
 * @since 1.0
 */
public class ULocation {
    /**
     * Method returning if a location matches coords to another
     * @param a location to check matching
     * @param b location to check matching
     * @return returns if locations are at same block
     */
    public static boolean sameLocation(Location a, Location b) {
        return a.getWorld().equals(b.getWorld()) &&
            (Double) a.getX() == b.getX() &&
            (Double) a.getY() == b.getX() &&
            (Double) a.getZ() == b.getZ();
    }

    /**
     * @return returns wether or not a block location is occupied
     */
    public static Predicate<Location> isUnoccupied() {
        return l -> {
            Block b = l.getBlock();
            return UPhasables.phasableBlockTypes.contains(b.getType());
        };
    }

    /**
     * Check wheter a block is unoccupied or phasable
     * @param l Location to check upon
     * @return returns wether or not a block location is occupied
     */
    public static boolean isOccupied(Location l) {
        return UPhasables.phasableBlockTypes.contains(l.getBlock().getType());
    }

    /**
     * @return A new location where X/Y/Z are the center of the block
     */
    public static Location toCenterLocation(Location loc) {
        Location centerLoc = loc.clone();
        centerLoc.setX(loc.getBlockX() + 0.5);
        centerLoc.setY(loc.getBlockY() + 0.5);
        centerLoc.setZ(loc.getBlockZ() + 0.5);
        return centerLoc;
    }

    public static String toLocationString(Location loc) {
        return loc.getWorld().getName() + "," +
            ((Double) loc.getX()).toString() + "," +
            ((Double) loc.getY()).toString() + "," +
            ((Double) loc.getZ()).toString();
    }

    public static Location fromString(String loc) {
        return new Location(
            Bukkit.getWorld(loc.split(",")[0]),
            Double.parseDouble(loc.split(",")[1]),
            Double.parseDouble(loc.split(",")[2]),
            Double.parseDouble(loc.split(",")[3])
        );
    }
}