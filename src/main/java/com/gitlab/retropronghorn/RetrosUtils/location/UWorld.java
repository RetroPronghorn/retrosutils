package com.gitlab.retropronghorn.retrosutils.location;

import java.util.function.Predicate;

import org.bukkit.Location;

/** Utility methods for checking worlds
 * @author RetroPronghorn
 * @since 1.0
 */
public class UWorld {
    /**
     * Check it a given world is the overworld
     * @return returns if the world is overworld
     */
    public static Predicate<? extends Location> isOverworld() {
        return w -> w.getWorld().toString().equalsIgnoreCase("world");
    }

    /**
     * Check it a given world is the overworld
     * @return returns if the world is overworld
     */
    public static Boolean isOverworld(Location l) {
        return l.getWorld().toString().equalsIgnoreCase("world");
    }

    /**
     * Check it a given world is the nether
     * @return returns if the world is nether
     */
    public static Predicate<? extends Location> isNether() {
        return w -> w.getWorld().toString().equalsIgnoreCase("world_nether");
    }

    /**
     * Check it a given world is the nether
     * @return returns if the world is nether
     */
    public static Boolean isNether(Location l) {
        return l.getWorld().toString().equalsIgnoreCase("world_nether");
    }

    /**
     * Check it a given world is the end
     * @return returns if the world is end
     */
    public static Predicate<? extends Location> isTheEnd() {
        return w -> w.getWorld().toString().equalsIgnoreCase("world_the_end");
    }

    /**
     * Check it a given world is the end
     * @return returns if the world is end
     */
    public static Boolean isTheEnd(Location l) {
        return l.getWorld().toString().equalsIgnoreCase("world_the_end");
    }

    /**
     * Check if a location is in a given world
     * @param l location to check against
     * @param world world we expect
     * @return returns wether or not this location is in the given world
     */
    public static Boolean isWorld(Location l, String world) {
        return l.getWorld().toString().equalsIgnoreCase(world);
    }
}