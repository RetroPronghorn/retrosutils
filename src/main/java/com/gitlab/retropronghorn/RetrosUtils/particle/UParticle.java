package com.gitlab.retropronghorn.retrosutils.particle;

import java.util.Random;

import com.gitlab.retropronghorn.retrosutils.server.ULogger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.plugin.java.JavaPlugin;

/** Utility methods for checking and building particles
 * @author RetroPronghorn
 * @since 1.0
 */
public class UParticle {
    public static final Particle FALLBACK = Particle.FLAME;
    private static final Particle[] PARTICLES = Particle.values();

    /**
     * Check wether a particle exists in this version of minecraft
     * @param p string value to check upon
     * @return returns wether or not a particle exists
     */
    public static Boolean exists(String p) {
        // Cache and iterate, it's faster.
        for (int i = 0; i < PARTICLES.length; i++) {
            if (PARTICLES[i].toString().equals(p)) return true;
        }
        return false;
    }

    /**
     * Get the particle from a string key else a fallback particle
     * @param p string key to convert to particle
     * @return a new particle from string key
     */
    public static Object particleOrFallback(String p) {
        if (!exists(p))
            ULogger.warning(
                "Retro's Utils",
                "Particle " + p + " does not exist, falling back to default particle."
            );
        return exists(p) ? Particle.valueOf(p) : FALLBACK;
    }

    public static void createHelix(Location location, Particle particle, JavaPlugin plugin) {
        int radius = 2;
        for(double i = 0; i <= 10; i+=0.05) {
            double y = i;
            double x = (radius-y) * Math.cos(y);
            double z = (radius-y) * Math.sin(y);
            Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                @Override
                public void run() {
                    location.getWorld().spawnParticle(
                        particle,
                        (float) (location.getX() + x),
                        (float) (location.getY() + y),
                        (float) (location.getZ() + z),
                        1
                    );
                }
            }, ((Double) ((10 - i)*3)).longValue());
        }
    }

    public static void createCloud(Location location, Random random, Particle particle, int count) {
        for (int i = 0; i <= count; i++) {
            double offsetX = random.nextDouble();
            double offsetY = random.nextDouble();
            double offsetZ = random.nextDouble();
            location.getWorld().spawnParticle(
                particle,
                (float) (location.getX() + offsetX),
                (float) (location.getY() + offsetY),
                (float) (location.getZ() + offsetZ),
                1
            );
        }
    }

}