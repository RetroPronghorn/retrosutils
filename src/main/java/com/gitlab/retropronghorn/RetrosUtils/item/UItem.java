package com.gitlab.retropronghorn.retrosutils.item;

import java.lang.reflect.Array;
import java.util.List;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class UItem {
    /**
     * Change UUID to minecraft color escaped string
     * @param uuid uuid string to escape
     * @return escaped UUID
     */
    private static String uuidToColor(String uuid){
        StringBuilder build = new StringBuilder();
        for(int i = 0; i < uuid.length(); i++){
            if(uuid.charAt(i) != '-') {
                build.append("§").append(uuid.charAt(i));
            } else {
                build.append("§k");
            }
        }
        build.append("§r§5");
        return build.toString();
    }

    /**
     * Add a color escaped UUID to a lore line. Item must have at least one line of lore.
     * @param i item to add lore to
     * @param uuid uuid to add
     * @return
     */
    public static ItemStack addHiddenUUID(ItemStack i, UUID uuid) {
        ItemMeta meta = i.getItemMeta();
        List<String> lore = meta.getLore();
        String serializedUUID = uuidToColor(uuid.toString());
        String hiddenUUID = lore.get(0) + serializedUUID;
        lore.set(0, hiddenUUID);
        meta.setLore(lore);
        i.setItemMeta(meta);
        return i;
    }

    /**
     * Get previously hidden UUID from an item
     * @return returns the parsed UUID
     */
    public static UUID getHiddenUUID(ItemStack i) {
        String loreLine = i.getItemMeta().getLore().get(0);
        String hiddenUUID = loreLine.split("§r§5")[0];
        hiddenUUID = hiddenUUID.replace("§k", "-");
        hiddenUUID = hiddenUUID.replace("§", "");
        return UUID.fromString(hiddenUUID);
    }

    /**
     * Create a custom itemstack with display name, lore and amount
     * @param type Material of the item
     * @param name Custom displayname of the item
     * @param lore lore array for the item
     * @return custom itemstack
     */
    public static ItemStack customItemStack(Material type, String name, List<String> lore) {
        return customItemStack(type, name, lore, 1);
    }

    /**
     * Create a custom itemstack with display name, lore and amount
     * @param type Material of the item
     * @param name Custom displayname of the item
     * @param lore lore array for the item
     * @param amount amount in the stack
     * @return custom itemstack
     */
    public static ItemStack customItemStack(Material type, String name, List<String> lore, int amount) {
        ItemStack item = new ItemStack(type);
        ItemMeta itemMeta = item.getItemMeta();
        item.setAmount(amount);
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        item.setItemMeta(itemMeta);
        return item;
    }
}