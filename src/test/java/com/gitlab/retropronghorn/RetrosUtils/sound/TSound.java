package com.gitlab.retropronghorn.retrosutils.sound;

import org.bukkit.Sound;
import org.junit.Test;

public class TSound {
    String invalidSound = "HISS";
    String validPart = "ENTITY_EXPERIENCE_ORB_PICKUP";
    Sound xpOrb = Sound.ENTITY_EXPERIENCE_ORB_PICKUP;

    @Test
    public void exists() {
        assert(!USound.exists(invalidSound));
        assert(USound.exists(validPart));
    }

    @Test
    public void materialOrFallback() {
        assert(USound.soundOrFallback(validPart).equals(xpOrb));
        assert(USound.soundOrFallback(invalidSound).equals(USound.FALLBACK));
    }
}