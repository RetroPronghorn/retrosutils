package com.gitlab.retropronghorn.retrosutils.material;

import org.bukkit.Material;
import org.junit.Test;

public class TMaterial {
    String invalidMat = "COPPER_BLOCK";
    String validMat = "IRON_BLOCK";
    Material ironBlock = Material.IRON_BLOCK;

    @Test
    public void exists() {
        assert(!UMaterial.exists(invalidMat));
        assert(UMaterial.exists(validMat));
    }

    @Test
    public void materialOrFallback() {
        assert(UMaterial.materialOrFallback(validMat).equals(ironBlock));
        assert(UMaterial.materialOrFallback(invalidMat).equals(UMaterial.FALLBACK));
    }
}