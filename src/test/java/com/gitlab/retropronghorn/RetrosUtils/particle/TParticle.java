package com.gitlab.retropronghorn.retrosutils.particle;

import org.bukkit.Particle;
import org.junit.Test;

public class TParticle {
    String invalidPart = "WINDEX";
    String validPart = "FLAME";
    Particle flame = Particle.FLAME;

    @Test
    public void exists() {
        assert(!UParticle.exists(invalidPart));
        assert(UParticle.exists(validPart));
    }

    @Test
    public void materialOrFallback() {
        assert(UParticle.particleOrFallback(validPart).equals(flame));
        assert(UParticle.particleOrFallback(invalidPart).equals(UParticle.FALLBACK));
    }
}